import React, { useState } from 'react';

const ordenarTabla = ({columna}) => {
    const [abogadosOrdenados, guardarAbogadosOrdenados] = useState([]);

    console.log(columna)
    let abogadosOrdenadosFuncion = [...abogados];
    switch (columna) {
        case 'tipo':
            abogadosOrdenadosFuncion.sort((a, b) => {
                if (a.tipo < b.tipo) {
                    return -1;
                }
                if (a.tipo > b.tipo) {
                    return 1;
                }
                return 0;
            })
            console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.tipo))
            guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        case 'nombre':
            abogadosOrdenadosFuncion.sort((a, b) => {
                if (a.tipo < b.tipo) {
                    return -1;
                }
                if (a.tipo > b.tipo) {
                    return 1;
                }
                return 0;
            })
            console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.tipo))
            guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        case 'especialidadesAbogado':
            abogadosOrdenadosFuncion.sort((a, b) => {
                if (a.tipo < b.tipo) {
                    return -1;
                }
                if (a.tipo > b.tipo) {
                    return 1;
                }
                return 0;
            })
            console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.tipo))
            guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        case 'direccion':
            abogadosOrdenadosFuncion.sort((a, b) => {
                if (a.tipo < b.tipo) {
                    return -1;
                }
                if (a.tipo > b.tipo) {
                    return 1;
                }
                return 0;
            })
            console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.tipo))
            guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        case 'contadorAsuntos':
            abogadosOrdenadosFuncion.sort((a, b) => {
                if (a.tipo < b.tipo) {
                    return -1;
                }
                if (a.tipo > b.tipo) {
                    return 1;
                }
                return 0;
            })
            console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.tipo))
            guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        case 'contadorRechazados':
            abogadosOrdenadosFuncion.sort((a, b) => {
                if (a.tipo < b.tipo) {
                    return -1;
                }
                if (a.tipo > b.tipo) {
                    return 1;
                }
                return 0;
            })
            console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.tipo))
            guardarAbogadosOrdenados(abogadosOrdenadosFuncion)


        default:
            return abogadosOrdenadosFuncion;
    }

    return abogadosOrdenados;

}

export default ordenarTabla;