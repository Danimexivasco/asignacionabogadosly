import React, {useState, useContext, useEffect} from 'react';
import Link from 'next/link';
import {format} from 'date-fns';

import {PendienteAsignar, Aceptado, Rechazado, PendienteAceptar} from '../ui/Estados';

import {FirebaseContext} from '../../firebase/index';

const Abogado = ({ oportunidad }) => {

    const [abogado, guardarAbogado] = useState({});
    const {firebase} = useContext(FirebaseContext);

    const { id, idOportunidad, abogadoAsignado, creador, especialidad, creado, nombreCliente, telefonoCliente, estado, provinciaOportunidad} = oportunidad;
    

    const buscarNombreAbogado = async () => {
        try {
            const abogadoDB = await firebase.db.collection('abogados').doc(abogadoAsignado).get()
            guardarAbogado(abogadoDB.data())
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(()=>{
        if(abogadoAsignado){
            buscarNombreAbogado()
        }
    },[abogadoAsignado])

    const hacerSwitch = (estado) => {
        switch (estado) {
            case "Pendiente Aceptar":
                return <PendienteAceptar>{estado}</PendienteAceptar>;
            case "Aceptada":
                return <Aceptado>{estado}</Aceptado>;
            case "Rechazada":
                return <Rechazado>{estado}</Rechazado>;
            case "Pendiente Asignar":
                return <PendienteAsignar>{estado}</PendienteAsignar>;
            default:
                return estado;
        }
    }

    const switchNombre = (abogado) => {
        switch (abogado.nombre, abogado.apellidos) {
            case (abogado.nombre === undefined || abogado.apellidos === undefined):
                
            return <span></span>
        
            default:
                return <span>{`${abogado.nombre} ${abogado.apellidos}`}</span>
        }
    }
    
    return (
        <tr className="text-center ">
            <th className="p-3 align-middle">
                <Link href="/oportunidades/[id]" as={`/oportunidades/${id}`}>
                    <a>{idOportunidad}</a>        
                </Link>
            </th>

            <th className="p-3 align-middle">{nombreCliente}</th>
            <th className="p-3 align-middle">{telefonoCliente}</th>
            <th className="p-3 align-middle">
                {hacerSwitch(estado)}
            </th>
            <th className="p-3 align-middle">{abogado.nombre && abogado.apellidos !== undefined ? (abogadoAsignado ? switchNombre(abogado) : '') : ''}</th>
            <th className="p-3 align-middle">{format(creado, 'dd/MM/yyyy HH:mm:ss')}</th>
            <th className="p-3 align-middle">{especialidad}</th>
            <th className="p-3 align-middle">{provinciaOportunidad}</th>
            <th className="p-3 align-middle">{creador.nombre}</th>
        </tr>


    );
}

export default Abogado;
