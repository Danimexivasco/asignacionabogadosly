import React, { useState, useEffect } from 'react';
import {format} from 'date-fns';

const SeleccionarAbogado = ({ abogado, seleccionarRadio, abogadoAsignado, abogadosRechazadores, checarNombreAbogado }) => {

    const [rechazador, guardarRechazador] = useState(false);
    const [clase, guardarClase] = useState('');

    const { tipo, nombre, apellidos, id, idLawyou, dni, direccion, calle, provincia, ciudad, especialidadesAbogado, comunidadAutonoma, cp, contadorAsuntos, contadorRechazados, ultimaOportunidad } = abogado;

    useEffect(() => {
        if (abogadosRechazadores !== undefined) {
            console.log(abogadosRechazadores)
            console.log(abogadoAsignado)
            console.log('RESULT RECHAZADORES: ',abogadosRechazadores.find(ab => ab === id) !== undefined)
            if (abogadosRechazadores.find(ab => ab === id) !== undefined) {
                // guardarRechazador(true)
                guardarClase('table-danger')
            } else {
                // guardarRechazador(false)
                guardarClase('')
            }

        }
    }, [abogadosRechazadores])

    console.log('ultimaOportunidad', ultimaOportunidad)
    return (
        <>
        <tr className= {`text-center ${clase}`}>
                {/* <th className="p-3 col-1 align-middle"> */}
                    {abogadoAsignado === id ? (
                        <th className="p-3 col-1 align-middle">
                        <input
                            type="radio"
                            name="abogadoAsignado"
                            value={id}
                            onChange={(e) => seleccionarRadio(e.target.value)}
                            defaultChecked
                        />
                        </th>

                    ) :
                        (
                            <th className="p-3 col-1 align-middle">
                            <input
                                type="radio"
                                name="abogadoAsignado"
                                value={id}
                                // onChange={(e) => seleccionarRadio(e.target.value)}
                                onChange={(e) => seleccionarRadio(e.target.value)}
                            />
                            </th>
                        )}

                {/* </th> */}
                <th className="p-3 align-middle">
                    <span className="mr-1">{nombre}</span><span>{apellidos}</span>
                </th>
                <th className="p-3 col-1 align-middle">{tipo}</th>
                <th className="p-3 align-middle">{especialidadesAbogado.map((especialidad, index) => {
                    return (<span key={`especialidad${index}`}>{(index ? ', ' : '') + especialidad}</span>)
                })}</th>
                <th className="p-3 align-middle">{direccion}</th>
                {ultimaOportunidad === "" ? (
                    <th className="p-3 align-middle">Sin asignar</th>
                ) : (
                    <th className="p-3 align-middle">{format(ultimaOportunidad, 'dd/MM/yyyy HH:mm:ss')}</th>
                )}
                <th className="p-3 col-1 align-middle">{contadorAsuntos}</th>
                <th className="p-3 col-1 align-middle">{contadorRechazados}</th>
            </tr>
           

        </>
    );
}

export default SeleccionarAbogado;