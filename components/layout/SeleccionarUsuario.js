import React, { useState, useContext } from 'react';

import {Aceptado, Rechazado, Administrador} from '../ui/Estados';

import {FirebaseContext} from '../../firebase/index';


const SeleccionarUsuario = ({ usuario }) => {

    const {firebase} = useContext(FirebaseContext);

    const {id, nombre, email} = usuario;

    const controlarPermisos = async(usuario)=>{
        if(usuario.admitido){
            await firebase.db.collection('usuarios').doc(usuario.id).update({admitido: false})
        }else{
            await firebase.db.collection('usuarios').doc(usuario.id).update({admitido: true})
        }

    }

    const cambioPermisos = (usuario) =>{
        if(usuario !== undefined){
            controlarPermisos(usuario);
        }
    }

    

    return (
        <>
            <tr key={usuario.email}>
                <td className="p-3 align-middle text-center">{usuario.nombre}</td>
                <td className="p-3 align-middle text-center">{usuario.email}</td>
                {usuario.rol === "superAdmin" ? (
                    <td className="p-3 align-middle text-center">
                        <Administrador
                            className="sobre"
                        >Administrador</Administrador>
                    </td>
                ) : (
                        usuario.admitido ? (
                            <td className="p-3 align-middle text-center">
                                <Aceptado
                                    className="sobre"
                                    onClick={() => cambioPermisos(usuario)}
                                >Autorizado</Aceptado>
                            </td>
                        ) : (
                                <td className="p-3 align-middle text-center">
                                    <Rechazado
                                        className="sobre"
                                        onClick={() => cambioPermisos(usuario)}
                                    >Desautorizado</Rechazado>
                                </td>
                            )
                    )}


            </tr>


        </>
    );
}

export default SeleccionarUsuario;