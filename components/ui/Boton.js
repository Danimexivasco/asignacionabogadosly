import styled from '@emotion/styled';

export const Boton = styled.button`
    padding: 0.75rem;
    margin-top: 2rem;
    margin-bottom: 4rem;
    font-size: 0.85rem;
 
`;