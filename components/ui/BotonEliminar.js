import styled from '@emotion/styled';

export const BotonEliminar = styled.button`
    padding: 0.75rem;
    font-size: 0.85rem;
`;