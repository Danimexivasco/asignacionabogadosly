import styled from '@emotion/styled';

export const OpcionesUsuario = styled.div`
    display: flex;
    align-items: center;
    margin-right: 2rem;
    margin-left: auto;
    p{
        color: whitesmoke;
        margin: 0 1.5rem;  
        font-size: 1rem;
    }
`;