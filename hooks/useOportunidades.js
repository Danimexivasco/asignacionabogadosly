import React, { useState, useEffect, useContext } from 'react';
import { FirebaseContext } from '../firebase/index';

const useOportunidades = (orden) => {

    const [oportunidades, guardarOportunidades] = useState([]);
    const { firebase } = useContext(FirebaseContext);

    useEffect(() => {
        let montado = true;
        console.log(montado)
        const obtenerOportunidades = () => {
            firebase.db.collection('oportunidades').orderBy(orden, 'desc').onSnapshot(manejarSnapShot)
        }
        if (montado) {
            obtenerOportunidades()
        }
        return () => montado = false;
        
    }, [])

    function manejarSnapShot(snapshot) {
        const oportunidadesDB = snapshot.docs.map(doc => {

            return {
                id: doc.id,
                ...doc.data()
            }
        })
        guardarOportunidades(oportunidadesDB);
    }

    return {
        oportunidades
    };
}

export default useOportunidades;