import React, {useState, useEffect} from 'react';
import firebase from '../firebase/index';

const useValidacion = (stateInicial, validar, funcion) => {

    const [valores, guardarValores] = useState(stateInicial);
    const [errores, guardarErrores] = useState({});
    const [submitForm, guardarSubmitForm] = useState(false);

    
    // Destructuring para trabajar con el DNI
    const {dni} = valores;

    useEffect(()=>{
        if(submitForm){
            const noErrores = Object.keys(errores).length === 0;
            if(noErrores){
                funcion();
            }
            guardarSubmitForm(false);
        }
    },[errores]);

    //************ Duplicado DNI**************** */
    
    const [testAbogados, guardarTestAbogados] = useState([]);

    useEffect(() => {
        const obtenerAbogados = () => {
            firebase.db.collection('abogados').onSnapshot(manejarSnapShot)
        }
        obtenerAbogados();
    }, [])

    function manejarSnapShot(snapshot) {
        const abogadosDB = snapshot.docs.map(doc => {
            return {
                id: doc.id,
                ...doc.data()
            }
        })

        guardarTestAbogados(abogadosDB);
    }

    useEffect(() => {
        if (testAbogados.length !== 0) {
            if(testAbogados.find(tes => tes.dni === dni) !== undefined){
                // console.log('duplicado')
                guardarValores({
                    ...valores,
                    dniDuplicado: true
                })
                return
            }
            guardarValores({
                ...valores,
                dniDuplicado: false
            })
            
        }
    }, [dni])

    //***************************** */

    useEffect(()=>{
        guardarValores(stateInicial)
    },[stateInicial])

    //***************************** */

    // Funcion que se ejecuta cuando se escribe algo
    const handleChange = e => {
        guardarValores({
            ...valores,
            [e.target.name]: e.target.value
        })
    }

    // Funcion que se ejecuta cuando se hace submit
    const handleSubmit = e => {
        e.preventDefault();
        const erroresValidacion = validar(valores);
        guardarErrores(erroresValidacion);
        guardarSubmitForm(true);
    }

       // Se ejecuta cuando se selecciona una provincia
       const handleChangeProvincia = (opcion) => {
        guardarValores({
            ...valores,
            provincia: opcion.value
        })
        // console.log(`Opcion seleccionada:`, opcion.value);
    };

      // Se ejecuta cuando se selecciona un tipo
      const handleChangeTipo = (opcion) => {
        guardarValores({
            ...valores,
            tipo: opcion.value
        })
    };

      // Se ejecuta cuando se selecciona un tipo
      const handleChangeComunidad = (opcion) => {
        guardarValores({
            ...valores,
            comunidadAutonoma: opcion.value
        })
    };

      // Se ejecuta cuando se selecciona una especialidad
      const handleChangeEspecialidad = (opciones) => {
        if (opciones === null) {
            opciones = [];
          }
        let resultado = opciones.map(especialidad => especialidad.value);
        guardarValores({
            ...valores,
            especialidadesAbogado: resultado
        })
    };
   
   

    return {
        valores,
        errores,
        submitForm,
        handleSubmit,
        handleChange,
        handleChangeProvincia,
        handleChangeTipo,
        handleChangeComunidad,
        handleChangeEspecialidad
        
    };
}
 
export default useValidacion;