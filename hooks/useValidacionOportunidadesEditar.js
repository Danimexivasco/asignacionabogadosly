import React, {useState, useEffect} from 'react';
import firebase from '../firebase/index';

const useValidacionOportunidadesEditar = (stateInicial, validar, funcion) => {
    

    const [valores, guardarValores] = useState(stateInicial);
    const [errores, guardarErrores] = useState({});
    const [submitForm, guardarSubmitForm] = useState(false);

    useEffect(()=>{
        if(submitForm){
            const noErrores = Object.keys(errores).length === 0;
            if(noErrores){
                funcion();
            }
            guardarSubmitForm(false);
        }
    },[errores]);

    
    //***************************** */

    useEffect(()=>{
        guardarValores(stateInicial)
        
    },[stateInicial])

    //***************************** */
    
    useEffect(()=>{
        nombreAbogado()
    },[valores.abogadoAsignado])


    async function nombreAbogado() {
        if (valores.abogadoAsignado) {
            const abogadoSeleccionado = await firebase.db.collection('abogados').doc(valores.abogadoAsignado).get()
            guardarValores({
                ...valores,
                nombreAbogadoAsignado: `${abogadoSeleccionado.data().nombre} ${abogadoSeleccionado.data().apellidos}`}
            )
        }
    }

    // Funcion que se ejecuta cuando se escribe algo
    const handleChange = e => {
        guardarValores({
            ...valores,
            [e.target.name]: e.target.value
        })
    }

    // Funcion que se ejecuta cuando se hace submit
    const handleSubmit = e => {
        e.preventDefault();
        const erroresValidacion = validar(valores);
        guardarErrores(erroresValidacion);
        guardarSubmitForm(true);
    }

       // Se ejecuta cuando se selecciona una provincia
       const handleChangeProvincia = (opcion) => {
        guardarValores({
            ...valores,
            provinciaOportunidad: opcion.value
        })
        // console.log(`Opcion seleccionada:`, opcion.value);
    };

      // Se ejecuta cuando se selecciona un tipo
      const handleChangeComunidad = (opcion) => {
        guardarValores({
            ...valores,
            comunidadAutonoma: opcion.value
        })
    };

      // Se ejecuta cuando se selecciona una especialidad
      const handleChangeEspecialidad = (opcion) => {
        guardarValores({
            ...valores,
            especialidad: opcion.value
        })
    };
   
    const handleChangeAbogadoAsignado = (opcion) =>{
        console.log(opcion)
        guardarValores({
            ...valores,
            abogadoAsignado: opcion
        })
    }
   
    const handleChangeEstado = (opcion) =>{
        console.log(opcion)
        guardarValores({
            ...valores,
            estado: opcion.value
        })
    }

     // Se ejecuta cuando se selecciona un motivo de rechazo
     const handleChangeMotivo = (opcion) => {
        guardarValores({
            ...valores,
            motivoRechazo: opcion.value
        })
    };

    return {
        valores,
        errores,
        submitForm,
        handleSubmit,
        handleChange,
        handleChangeProvincia,
        handleChangeComunidad,
        handleChangeEspecialidad,
        handleChangeAbogadoAsignado,
        handleChangeEstado,
        handleChangeMotivo
        
    };
}
 
export default useValidacionOportunidadesEditar;