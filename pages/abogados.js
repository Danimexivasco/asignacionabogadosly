import React, { useContext, useState, useEffect } from 'react';
import Layout from '../components/layout/Layout';
import Link from 'next/link';
import styled from '@emotion/styled';
import ReactTooltip from 'react-tooltip';


import PaginaError from '../components/layout/PaginaError';
import { Boton } from '../components/ui/Boton';
import DetallesAbogado from '../components/layout/DetallesAbogado';
import useAbogados from '../hooks/useAbogados';
import { SinResultados } from '../components/ui/SinResultados';
import { InputText, InputSubmit } from '../components/ui/Busqueda';
import ordenarTabla from '../components/helpers/ordenarTabla';

import { FirebaseContext } from '../firebase/index';



const Abogados = () => {

  // const [columnaSeleccionada, guardarColumnaSeleccionada] = useState('nombre');
  const [abogadosOrdenados, guardarAbogadosOrdenados] = useState([]);
  const [permiso, guardarPermiso] = useState(false);
  const [cargando, guardarCargando] = useState(false);


  const { usuario, firebase } = useContext(FirebaseContext);
  const { abogados } = useAbogados('nombre');

  ////////////////

  // buscar usuario
  const busqueda = async (email) => {
    await firebase.db.collection('usuarios').where("email", "==", email).onSnapshot(manejarSnapShot);
  }

  function manejarSnapShot(snapshot) {
    const usuarioBD = snapshot.docs.map(doc => {
      return {
        id: doc.id,
        ...doc.data()
      }
    })
    if (usuarioBD[0] !== undefined) {
      // console.log("USUARIO DB", usuarioBD[0])
      if (usuarioBD[0].admitido) {
        guardarPermiso(true);
      } else {
        guardarPermiso(false)
      }
    }
    guardarCargando(false);
  }

  useEffect(() => {
    guardarCargando(true);
    if (usuario !== undefined && usuario !== null) {
      console.log(usuario.email)
      busqueda(usuario.email)
    }
  }, [usuario])

  ////////////
  // FILTRADO
  const [q, guardarQ] = useState('');
  const [resultados, guardarResultados] = useState([])


  useEffect(() => {
    if (abogados) {
      const busqueda = q.toLowerCase();
      const busquedaSinTildes = quitarTildes(busqueda);
      if (abogadosOrdenados.length !== 0) {
        const abogadosFiltrados = abogadosOrdenados.filter(abogado => {
          return (
            `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(abogado.apellidos.toLowerCase())}`.includes(busquedaSinTildes)
            || quitarTildes(abogado.tipo.toLowerCase()).includes(busquedaSinTildes) || (arrayMinusculas(abogado.especialidadesAbogado).some(function (v) { return v.indexOf(busquedaSinTildes) >= 0 }) ? abogado.especialidadesAbogado : null)
          )
        })
        guardarResultados(abogadosFiltrados)
      } else {
        // console.log('TEST', abogados.filter(abogado=> (arrayMinusculas(abogado.especialidadesAbogado).some(function(v){ return v.indexOf(busquedaSinTildes)>=0 }) ? abogado.especialidadesAbogado : null)))
        const abogadosFiltrados = abogados.filter(abogado => {
          return (
            `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(abogado.apellidos.toLowerCase())}`.includes(busquedaSinTildes)
            || quitarTildes(abogado.tipo.toLowerCase()).includes(busquedaSinTildes)
            || (arrayMinusculas(abogado.especialidadesAbogado).some(function (v) { return v.indexOf(busquedaSinTildes) >= 0 }) ? abogado.especialidadesAbogado : null)

          )
        })
        guardarResultados(abogadosFiltrados)
      }
    }

  }, [q, abogados, abogadosOrdenados])

  const quitarTildes = (palabra) => {
    const letras = { 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u' };
    palabra = palabra.replace(/[áéíóú]/g, m => letras[m]);

    return palabra;
  }

  const arrayMinusculas = (array) => {
    const especialidadesMinusculas = []

    // console.log('array', array)
    for (let i = 0; i < array.length; i++) {
      const especialidad = quitarTildes(array[i])
      const especialidadMinuscula = especialidad.toLowerCase()
      // console.log('especialidadMinuscula', especialidadMinuscula)
      especialidadesMinusculas.push(especialidadMinuscula)
    }
    // console.log('especialidadesMinusculas', especialidadesMinusculas)
    return especialidadesMinusculas;

  }

  const ordenarTabla = (columna) => {
    console.log(columna)
    let abogadosOrdenadosFuncion = [...abogados];
    switch (columna) {
      case 'tipo':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.tipo < b.tipo) {
            return -1;
          }
          if (a.tipo > b.tipo) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.tipo))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;
      case 'nombre':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.nombre < b.nombre) {
            return -1;
          }
          if (a.nombre > b.nombre) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.nombre))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;
      case 'especialidadesAbogado':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.especialidadesAbogado < b.especialidadesAbogado) {
            return -1;
          }
          if (a.especialidadesAbogado > b.especialidadesAbogado) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.especialidadesAbogado))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;
      case 'direccion':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.direccion < b.direccion) {
            return -1;
          }
          if (a.direccion > b.direccion) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.direccion))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;
      case 'contadorAsuntos':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.contadorAsuntos < b.contadorAsuntos) {
            return -1;
          }
          if (a.contadorAsuntos > b.contadorAsuntos) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.contadorAsuntos))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;
      case 'contadorRechazados':
        abogadosOrdenadosFuncion.sort((a, b) => {
          if (a.contadorRechazados < b.contadorRechazados) {
            return -1;
          }
          if (a.contadorRechazados > b.contadorRechazados) {
            return 1;
          }
          return 0;
        })
        console.log('Abogados ordenados: ', abogadosOrdenadosFuncion.map(abogado => abogado.contadorRechazados))
        guardarAbogadosOrdenados(abogadosOrdenadosFuncion)
        break;

      default:
        return abogadosOrdenadosFuncion;
    }
  }


  return (
    <div>
      <Layout>

        {usuario && permiso ? (

          <>
            <h1 className="titulo">Abogados</h1>
            <div className="d-flex justify-content-start align-center">
              <Link href="/nuevo-abogado">
                <Boton
                  className="btn btn-primary col-2"
                >Nuevo Abogado</Boton>
              </Link>
              <div className="d-flex mt-4">
                <InputText
                  type="text"
                  value={q}
                  onChange={e => guardarQ(e.target.value)}
                  placeholder="Busca algo..."
                  className="mt-2"
                  data-tip="Busca por Nombre, Tipo o Especialidades"
                />
                <InputSubmit disabled>Buscar</InputSubmit>
                <ReactTooltip effect="solid" />
              </div>
            </div>

            {abogados.length === 0 ? <p>Cargando...</p> : (

              <div className="tablaScroll">
              <table className="table table-bordered table-hover w-100">

                <thead className="thead-dark text-center p-3">
                  <tr className="m-2">
                    <th className="align-middle cabecera" onClick={() => ordenarTabla('nombre')}>Nombre y Apellidos</th>
                    <th className="align-middle cabecera" onClick={() => ordenarTabla('tipo')}>Tipo</th>
                    <th className="align-middle cabecera" onClick={() => ordenarTabla('especialidadesAbogado')}>Especialidades</th>
                    <th className="align-middle cabecera" onClick={() => ordenarTabla('direccion')}>Dirección</th>
                    <th className="align-middle cabecera" onClick={() => ordenarTabla('contadorAsuntos')}>Asuntos Llevados</th>
                    <th className="align-middle cabecera" onClick={() => ordenarTabla('contadorRechazados')}>Asuntos Rechazados</th>
                  </tr>
                </thead>

                <tbody>
                  {/* {Object.keys(resultados).length === 0 ? <tr className="text-center font-weight-bold" ><td colSpan="6">Cargando...</td></tr> : ( */}

                  {resultados.length === 0 ? <SinResultados className="text-center" ><td colSpan="6">La búsqueda no dió resultados</td></SinResultados> : (


                    resultados.map(abogado => {

                      return (
                        <DetallesAbogado
                          key={abogado.id}
                          abogado={abogado}

                        />
                      )
                    })

                  )
                  }

                  {/* {abogados.length === 0 ? <tr><th>No hay abogados</th></tr> : (

                  
                    abogados.map(abogado => {

                      return (
                        <Abogado
                          key={abogado.id}
                          abogado={abogado}

                        />
                      )
                    })
                  
                )} */}

                </tbody>

              </table>
              </div>

            )}
          </>
        ) : !permiso && usuario && cargando ? (
          <p>Cargando...</p>
        )
            : !permiso && usuario && !cargando ? (
              <PaginaError msg={`No puedes pasar.`}></PaginaError>
            )
              : !permiso && !usuario ? (
                <>
                  <PaginaError msg={`Tienes que estar logueado.`}></PaginaError>
                  <div className="text-center">
                    <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                  </div>
                </>

              ) : (
                  <>
                    <PaginaError msg={`Tienes que estar logueado.`}></PaginaError>
                    <div className="text-center">
                      <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                    </div>
                  </>
                )}



      </Layout>
    </div>
  )
}

export default Abogados;
