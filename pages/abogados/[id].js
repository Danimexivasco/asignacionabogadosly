import React, { useContext, useState, useEffect } from 'react';
import Link from 'next/link';
import Select from 'react-select';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';

import PaginaError from '../../components/layout/PaginaError';
import Layout from '../../components/layout/Layout';
import { ContenedorFormularioLg, BotonCancelar, BotonAceptar } from '../../components/ui/Formulario';
import { BotonEliminar } from '../../components/ui/BotonEliminar';
import {
    provincias,
    tipos,
    comunidades,
    especialidades
} from '../../components/ui/OpcionesSelectores';

// Validaciones
import useValidacionEditar from '../../hooks/useValidacionEditar';
import validarEditarAbogado from '../../validacion/validarEditarAbogado';

import { FirebaseContext } from '../../firebase/index';



const Abogado = () => {

    // State del componente
    const [abogado, guardarAbogado] = useState({});
    const [error, guardarError] = useState(false);
    const [consultarDB, guardarConsultarDB] = useState(true)

    // Routing para obtener el ID actual
    const router = useRouter();
    const { query: { id } } = router;

    // Context de Firebase
    const { firebase, usuario } = useContext(FirebaseContext);

    useEffect(() => {
        // Declaramos variable para trackear
        let estaCancelado = false;
        // console.log('useEffect [ID].js')

        if (id && consultarDB) {
            const obtenerAbogado = async () => {
                const abogadoQuery = await firebase.db.collection('abogados').doc(id);
                const abogado = await abogadoQuery.get();

                // Se ejecuta solo si esta montado
                if (!estaCancelado) {
                    if (abogado.exists) {
                        guardarAbogado(abogado.data());
                    } else {
                        guardarError(true);
                    }
                    guardarConsultarDB(false);
                }

            }
            obtenerAbogado();
        }
        return () => {
            // Ponemos la variable a true para evitar que intente actualizar un state inexistente
            estaCancelado = true;
        }
    }, [id, abogado])


    // if (Object.keys(producto).length === 0) return <p>Cargando...</p>

    const dniOriginal = abogado.dni;

    const { valores, errores, handleSubmit, handleChange, handleChangeTipo, handleChangeProvincia, handleChangeComunidad, handleChangeEspecialidad } = useValidacionEditar(abogado, dniOriginal, validarEditarAbogado, editarAbogado);


    // const { tipo, nombre, apellidos, idLawyou, dni, direccion, calle, provincia, ciudad, comunidadAutonoma, cp, creado, creador, contadorAsuntos, contadorRechazados } = abogado;


    // Destructuring de los valores
    const { tipo, nombre, apellidos, idLawyou, dni, direccion, calle, provincia, especialidadesAbogado, ciudad, comunidadAutonoma, cp, creado, creador, contadorAsuntos, contadorRechazados, ultimaOportunidad, emailAbogado } = valores;




    const valorProvincia = provincias.find(prov => prov.value === provincia);
    const valorTipo = tipos.find(tip => tip.value === tipo);
    const valorComunidad = comunidades.find(comunidad => comunidad.value === comunidadAutonoma);
    // const valorEspecialidades = especialidades.filter(especialidad => especialidad.value === especialidadesAbogado);
    const valorEspecialidades = [];
    if (especialidadesAbogado !== undefined) {
        // console.log(especialidadesAbogado)
        for (let i = 0; i < especialidadesAbogado.length; i++) {
            valorEspecialidades.push(especialidades.find(especialidad => especialidad.value === especialidadesAbogado[i]))
        }
    }



    const eliminarAbogado = async () => {
        if (!usuario) {
            return router.push("/login");
        }

        // Modal de SWAL
        Swal.fire({
            title: '¿Estás seguro?',
            text: "No podrás deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                try {
                    firebase.db.collection('abogados').doc(id).delete();
                    Swal.fire({
                        icon: 'success',
                        title: 'El abogado ha sido eliminado!',
                        showConfirmButton: false,
                        timer: 1200
                    })
                    setTimeout(() => {
                        router.push("/abogados")
                    }, 1200);
                } catch (error) {
                    console.log(error)
                }

            }
        })

    }


    async function editarAbogado() {
        if (!usuario) {
            return router.push('/iniciar-sesion');
        }

        // Creamos el objeto de abogado
        const abogado = {
            tipo,
            nombre,
            apellidos,
            idLawyou,
            dni,
            calle,
            provincia,
            ciudad,
            emailAbogado,
            direccion: `${calle}, ${ciudad}, ${provincia}, ${comunidadAutonoma} - ${cp}`,
            comunidadAutonoma,
            cp,
            especialidadesAbogado,
            actualizado: Date.now(),
            actualizaUsuario: {
                id: usuario.uid,
                nombre: usuario.displayName
            },
            creado,
            creador,
            contadorAsuntos,
            contadorRechazados,
            ultimaOportunidad
        };

        try {
            if (abogado) {
                // Actualiza en la bbdd
                await firebase.db.collection('abogados').doc(id).update(abogado);
                console.log('Editando abogado', abogado)
            }
            Swal.fire({
                icon: 'success',
                title: 'El abogado se editó correctamente!',
                showConfirmButton: false,
                timer: 1200
            })

            router.push('/abogados')
        } catch (error) {
            console.log(error)
            guardarError(error)
        }


    };

    return (
        <div>
            <Layout>

                {error ? <PaginaError msg="Vaya... Parece que ha habido un error al editar el abogado, vuelve a intentarlo por favor"></PaginaError> : Object.keys(valores).length === 0 ? <p>Cargando...</p> : (

                    usuario ? (
                        <>
                            <div className="col-12 d-flex justify-content-between">
                                <h1 className="titulo">Editar Datos Abogado</h1>
                                <BotonEliminar
                                    className="col-2 btn btn-danger"
                                    onClick={() => eliminarAbogado()}
                                >Eliminar Abogado</BotonEliminar>
                            </div>
                            <form
                                onSubmit={handleSubmit}
                            >
                                <ContenedorFormularioLg>
                                    <fieldset>
                                        <legend>Datos Personales</legend>
                                        <div className="col-12 d-flex mt-2">
                                            <div className="col-4">
                                                <label htmlFor="tipo">Tipo</label>
                                                <Select
                                                    onChange={handleChangeTipo}
                                                    options={tipos}
                                                    name="tipo"
                                                    placeholder="Selecciona Tipo"
                                                    defaultValue={valorTipo}
                                                />
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="idLawyou">ID Lawyou</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="idLawyou"
                                                    onChange={handleChange}
                                                    defaultValue={idLawyou}
                                                    placeholder="Ej. A050"
                                                />
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="dni">DNI</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="dni"
                                                    onChange={handleChange}
                                                    defaultValue={dni}
                                                    placeholder="DNI del Abogado"
                                                />
                                                <small>Ej. 12345678A</small>
                                                {errores.dni && <p className="alert alert-danger text-center p-2 mt-2">{errores.dni}</p>}
                                                {errores.dniDuplicado && <p className="alert alert-danger text-center p-2 mt-2">{errores.dniDuplicado}</p>}
                                            </div>

                                        </div>

                                        <div className="col-12 d-flex abajo">
                                            <div className="col-4">
                                                <label htmlFor="nombre">Nombre</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="nombre"
                                                    onChange={handleChange}
                                                    defaultValue={nombre}
                                                    placeholder="Nombre del abogado"
                                                />
                                                {errores.nombre && <p className="alert alert-danger text-center p-2 mt-2">{errores.nombre}</p>}
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="apellidos">Apellidos</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="apellidos"
                                                    onChange={handleChange}
                                                    defaultValue={apellidos}
                                                    placeholder="Apellidos del abogado"
                                                />
                                                {errores.apellidos && <p className="alert alert-danger text-center p-2 mt-2">{errores.apellidos}</p>}
                                            </div>

                                            <div className="col-4">
                                                <label htmlFor="especialidades">Especialidades</label>
                                                <Select
                                                    name="especialidades"
                                                    options={especialidades}
                                                    isMulti
                                                    onChange={handleChangeEspecialidad}
                                                    placeholder="Selecciona Especialidades"
                                                    defaultValue={valorEspecialidades}
                                                />
                                                {errores.especialidades && <p className="alert alert-danger text-center p-2 mt-2">{errores.especialidades}</p>}
                                            </div>

                                        </div>

                                        <div className="col-12 d-flex abajo">
                                            <div className="col-4">
                                            <label htmlFor="emailAbogado">Email</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="emailAbogado"
                                                onChange={handleChange}
                                                defaultValue={emailAbogado}
                                                placeholder="Email"
                                            />
                                            {errores.emailAbogado && <p className="alert alert-danger text-center p-2 mt-2">{errores.emailAbogado}</p>}
                                            </div>
                                        </div>

                                    </fieldset>

                                    <fieldset className="mt-5">
                                        <legend>Datos de Facturación</legend>
                                        <div className="col-12 d-flex mt-2 justify-content-between">
                                            <div className="col-6">
                                                <label htmlFor="calle">Calle</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="calle"
                                                    onChange={handleChange}
                                                    defaultValue={calle}
                                                    placeholder="Ej. Calle de Miramon Nº9, 3-A"
                                                />
                                                {errores.calle && <p className="alert alert-danger text-center p-2 mt-2">{errores.calle}</p>}
                                            </div>
                                            <div className="col-6">
                                                <label htmlFor="ciudad">Ciudad</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="ciudad"
                                                    onChange={handleChange}
                                                    defaultValue={ciudad}
                                                    placeholder="Ej. Sevilla"
                                                />
                                                {errores.ciudad && <p className="alert alert-danger text-center p-2 mt-2">{errores.ciudad}</p>}
                                            </div>
                                        </div>

                                        <div className="col-12 d-flex  abajo">
                                            <div className="col-4">
                                                <label htmlFor="provincia">Provincia</label>
                                                <Select
                                                    onChange={handleChangeProvincia}
                                                    options={provincias}
                                                    name="provincia"
                                                    placeholder="Selecciona Provincia"
                                                    defaultValue={valorProvincia}
                                                />
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="comunidadAutonoma">Comunidad Autónoma</label>
                                                <Select
                                                    onChange={handleChangeComunidad}
                                                    options={comunidades}
                                                    name="comunidadAutonoma"
                                                    placeholder="Selecciona Comunidad Autonoma"
                                                    defaultValue={valorComunidad}

                                                />
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="cp">Código Postal</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="cp"
                                                    onChange={handleChange}
                                                    defaultValue={cp}
                                                    placeholder="Ej. 20532"
                                                />
                                                {errores.cp && <p className="alert alert-danger text-center p-2 mt-2">{errores.cp}</p>}
                                            </div>
                                        </div>
                                    </fieldset>

                                </ContenedorFormularioLg>

                                <div className="col-12 d-flex justify-content-center">
                                    {error && <p className="alert alert-danger text-center p2 mt-2 mb-2">{error}</p>}
                                    <Link href="/abogados">
                                        <BotonCancelar
                                            className="btn btn-secondary"
                                        >Cancelar</BotonCancelar>
                                    </Link>

                                    <BotonAceptar
                                        className="btn btn-primary"
                                        type="submit"
                                    >Guardar Cambios</BotonAceptar>
                                </div>
                            </form>



                        </>
                    ) :
                        <>
                            <PaginaError msg={`Tienes que estar logueado para acceder a esta sección.`}></PaginaError>
                            <div className="text-center">
                                <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                            </div>
                        </>

                )}


            </Layout>
        </div>
    )
}

export default Abogado;

