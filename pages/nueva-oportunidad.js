import React, { useContext, useState, useEffect } from 'react';
// import { useRouter } from 'next/router';
import Router from 'next/router';
import Link from 'next/link';
import Select from 'react-select';
import Swal from 'sweetalert2';
import emailjs from 'emailjs-com';

import Layout from '../components/layout/Layout';
import { ContenedorFormularioLg, BotonAceptar, BotonCancelar } from '../components/ui/Formulario';
import PaginaError from '../components/layout/PaginaError';
import {
    provincias,
    comunidades,
    especialidades,
    comunidadesConProvincias
} from '../components/ui/OpcionesSelectores';
import useAbogadosSugerencia from '../hooks/useAbogadosSugerencia';
import { SinResultados } from '../components/ui/SinResultados';
import SeleccionarAbogado from '../components/layout/SeleccionarAbogado';
import { InputText, InputSubmit, InputTextOportunidades, InputSubmitSugerencias } from '../components/ui/Busqueda';
import useContadorOportunidades from '../hooks/useContadorOportunidades';

// Validaciones
import useValidacionOportunidades from '../hooks/useValidacionOportunidades';
import validarNuevaOportunidad from '../validacion/validarNuevaOportunidad';

// Firebase
import { FirebaseContext } from '../firebase/index';


const STATE_INICIAL = {
    idOportunidad: '',
    nombreCliente: '',
    telefonoCliente: '',
    especialidad: '',
    provinciaOportunidad: '',
    abogadoAsignado: '',
    estado: 'Pendiente Aceptar',
    numeroReasignaciones: 0,
    abogadosRechazadores: []

}

const NuevaOportunidad = () => {

    const [error, guardarError] = useState(false);
    const [errorDB, guardarErrorDB] = useState('');
    const [contadorAbogado, guardarContadorAbogado] = useState(0);
    const [otrasSugerencias, guardarOtrasSugerencias] = useState([]);
    const [contadorDB, guardarContadorDB] = useState(0);
    const [idOportunidad, guardarIdOportunidad] = useState('');
    const [comunidadDeProvincia, guardarComunidadDeProvincia] = useState('');
    const [abogadosPorComunidad, guardarAbogadosPorComunidad] = useState([]);
    const [sociosComunidad, guardarSociosComunidad] = useState([]);
    const [colaboradoresComunidad, guardarColaboradoresComunidad] = useState([]);
    const [sociosNacionales, guardarSociosNacionales] = useState([]);
    const [colaboradoresNacionales, guardarColaboradoresNacionales] = useState([]);
    const [emailAbogadoAsignado, guardarEmailAbogadoAsignado] = useState('');

    // FILTRADO
    const [abogadosSugeridos, guardarAbogadosSugeridos] = useState([]);
    const [colaboradoresSugeridos, guardarColaboradoresSugeridos] = useState([]);
    const [sinSugerencias, guardarSinSugerencias] = useState(false);
    const [busquedaManual, guardarBusquedaManual] = useState(false);

    const { firebase, usuario } = useContext(FirebaseContext);
    const { abogados } = useAbogadosSugerencia('contadorAsuntos');
    const { contador } = useContadorOportunidades();



    // const {provincia, especialidadesAbogado} = abogados;
    // if(abogados.length !== 0){

    //     console.log(abogados)
    // }

    const { valores, errores, handleSubmit, handleChange, handleChangeProvincia, handleChangeComunidad, handleChangeEspecialidad, handleChangeAbogadoAsignado, handleChangeContador, nombreAbogado} = useValidacionOportunidades(STATE_INICIAL, validarNuevaOportunidad, crearNuevaOportunidad);
    const { nombreCliente, telefonoCliente, especialidad, provinciaOportunidad, abogadoAsignado, estado, nombreAbogadoAsignado } = valores;
    console.log("VALORES", valores)

    // const {router} = useRouter();


    useEffect(() => {
        let montado = true;
        console.log(montado)
        if (contador !== undefined) {
            if (montado) {
                guardarContadorDB(contador.contador);
                console.log('contadorDB', contador.contador)
            }
        }
        return () => montado = false;
    }, [contador])

    // Sugerencias de SOCIOS y COLABORADORES
    useEffect(() => {
        let montado = true;
        if (montado && provinciaOportunidad !== '') {
            console.log("comunidadesConProvincias", comunidadesConProvincias)
            const comuna = comunidadesConProvincias.find(prov => prov.prov.includes(provinciaOportunidad))
            console.log(comuna.com)
            guardarComunidadDeProvincia(comuna.com)

        }

        if (abogados) {
            // console.log('abogados', abogados)
            const provinciaMinuscula = provinciaOportunidad.toLowerCase();
            const provinciaSinTildes = quitarTildes(provinciaMinuscula);
            // const especialidadMinuscula = especialidad.toLowerCase();
            // const especialidadSinTildes = quitarTildes(especialidadMinuscula);
            const abogadosFiltrados = abogados.filter(abogado => {
                if (especialidad !== '' && provinciaOportunidad !== '') {
                    return (
                        quitarTildes(abogado.provincia.toLowerCase()).includes(provinciaSinTildes) && abogado.especialidadesAbogado.includes(especialidad) && abogado.tipo !== 'Colaborador'
                    )
                }
            })
            const colaboradoresFiltrados = abogados.filter(abogado => {
                if (especialidad !== '' && provinciaOportunidad !== '') {
                    return (
                        quitarTildes(abogado.provincia.toLowerCase()).includes(provinciaSinTildes) && abogado.especialidadesAbogado.includes(especialidad) && abogado.tipo === 'Colaborador'
                    )
                }
            })
            if (montado) {
                guardarAbogadosSugeridos(abogadosFiltrados)
                console.log('sociosSugeridos', abogadosFiltrados)
                guardarColaboradoresSugeridos(colaboradoresFiltrados)
                console.log('colaboradoresSugeridos', colaboradoresFiltrados)
                if (especialidad !== '' && provinciaOportunidad !== '' && abogadosFiltrados.length === 0 && colaboradoresFiltrados.length === 0) {
                    guardarSinSugerencias(true);
                    guardarBusquedaManual(true);
                    return;
                }
                guardarSinSugerencias(false);
                guardarBusquedaManual(false);
            }
        }
        return () => montado = false;
    }, [provinciaOportunidad, especialidad])


    async function verContador() {
        if (abogadoAsignado) {
            const abogadoSeleccionado = await firebase.db.collection('abogados').doc(abogadoAsignado).get()
            guardarContadorAbogado(abogadoSeleccionado.data().contadorAsuntos);
            nombreAbogado(`${abogadoSeleccionado.data().nombre} ${abogadoSeleccionado.data().apellidos}`);
            guardarEmailAbogadoAsignado(abogadoSeleccionado.data().emailAbogado);
        }
    }

    useEffect(() => {
        let montado = true;
        if (montado) {
            verContador();
        }
        return () => montado = false;
    }, [abogadoAsignado]);

    const quitarTildes = (palabra) => {
        const letras = { 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u' };
        palabra = palabra.replace(/[áéíóú]/g, m => letras[m]);
        return palabra;
    }

    // Funcion para checar el abogado seleccionado
    const seleccionarRadio = async (opcion) => {
        handleChangeAbogadoAsignado(opcion);
    }


    // Funcion de filtrado manual
    // FILTRADO
    const [q, guardarQ] = useState('');
    const [resultados, guardarResultados] = useState([])

    const [qComunidad, guardarQComunidad] = useState('');




    // Cuando no hay abogados en la misma provincia
    useEffect(() => {
        let montado = true;
        console.log(montado)
        if (otrasSugerencias) {
            console.log("otrasSugerencias", otrasSugerencias)
            const busqueda = q.toLowerCase();
            const busquedaSinTildes = quitarTildes(busqueda);

            const abogadosFiltrados = otrasSugerencias.filter(abogado => {
                return (
                    `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(abogado.apellidos.toLowerCase())}`.includes(busquedaSinTildes)

                )
            })



            if (montado) {
                if (comunidadDeProvincia !== "") {
                    const busqueda = qComunidad.toLowerCase();
                    const busquedaSinTildes = quitarTildes(busqueda);
                    const abogadosComunidadAutonoma = otrasSugerencias.filter(abogado => {
                        return (
                            quitarTildes(abogado.comunidadAutonoma).includes(quitarTildes(comunidadDeProvincia)) && `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(abogado.apellidos.toLowerCase())}`.includes(busquedaSinTildes)
                        )
                    })
                    console.log("abogadosPorComunidad", abogadosComunidadAutonoma)
                    // Guardamos los abogados de la CCAA en state
                    guardarAbogadosPorComunidad(abogadosComunidadAutonoma)

                }
                const sociosNacionalesFiltrados = abogadosFiltrados.filter(abogado => {
                    return (
                        abogado.tipo !== 'Colaborador'
                    )
                })
                const colaboradoresNacionalesFiltrados = abogadosFiltrados.filter(abogado => {
                    return (
                        abogado.tipo === 'Colaborador'
                    )
                })
                guardarSociosNacionales(sociosNacionalesFiltrados)
                guardarColaboradoresNacionales(colaboradoresNacionalesFiltrados)
                guardarResultados(abogadosFiltrados)
            }
            console.log('abogadosFiltrados especialidad', abogadosFiltrados)
        }
        return () => montado = false;

    }, [q, qComunidad, otrasSugerencias, comunidadDeProvincia, provinciaOportunidad])

    // Diferenciamos Socios de Colaboradores (CCAA) cuando no haya sugerencias 
    useEffect(() => {
        let montado = true;
        if (montado && abogadosPorComunidad.length !== 0) {

            const sociosFiltrados = abogadosPorComunidad.filter(abogado => {
                return (
                    abogado.tipo !== 'Colaborador'
                )
            })
            console.log("sociosFiltrados", sociosFiltrados);
            guardarSociosComunidad(sociosFiltrados);

            const colaboradoresFiltrados = abogadosPorComunidad.filter(abogado => {
                return (
                    abogado.tipo === 'Colaborador'
                )
            })
            console.log("colaboradoresFiltrados", colaboradoresFiltrados);
            guardarColaboradoresComunidad(colaboradoresFiltrados);

        } else {
            guardarSociosComunidad([]);
            guardarColaboradoresComunidad([]);
        }

        return () => montado = false;
    }, [abogadosPorComunidad])

    const resultadosProvincia = async () => {
        await firebase.db.collection('abogados').where("especialidadesAbogado", "array-contains", especialidad).onSnapshot(manejarSnapShot);
    }

    useEffect(() => {
        resultadosProvincia();
    }, [especialidad])

    function manejarSnapShot(snapshot) {
        const abogadosDB = snapshot.docs.map(doc => {
            return {
                id: doc.id,
                ...doc.data()
            }
        })
        // const abogadosOrdenados = abogadosDB.sort((a, b) => (a.contadorAsuntos > b.contadorAsuntos) ? 1 : -1)
        const abogadosOrdenados = abogadosDB.sort((a, b) => (a.ultimaOportunidad > b.ultimaOportunidad) ? 1 : -1)
        guardarOtrasSugerencias(abogadosOrdenados);
    }

    useEffect(() => {
        let montado = true;
        if (contadorDB !== 0) {
            const fecha = new Date();
            const año = fecha.getFullYear();
            if (montado) {
                guardarIdOportunidad(`LY${año}${contadorDB}`)
            }
        }
        return () => montado = false;
    }, [contadorDB])


    const template_params = {
        "reply_to": "servicios.lawyou@lawyoulegal.com",
        "emailAbogado": emailAbogadoAsignado,
        "destinatario": nombreAbogadoAsignado,
        "from_name": "Lawyou",
        "mensaje_html": `<ul>
                            <li><b>Nombre Cliente</b>:  ${nombreCliente}</li>
                            <li><b>Telefono</b>:  ${telefonoCliente}</li>
                            <li><b>Especialidad</b>:  ${especialidad}</li>
                            <li><b>Provincia</b>:  ${provinciaOportunidad}</li>
                        </ul><br>
                        <p>En caso de no haber confirmado tu disponibilidad, no olvides hacerlo llamando al 667 60 66 11 o respondiendo a este mail</p>`
     }

    async function crearNuevaOportunidad() {
        if (!usuario) {
            return Router.push('/iniciar-sesion');
        }


        const oportunidad = {
            idOportunidad,
            nombreCliente,
            telefonoCliente,
            especialidad,
            provinciaOportunidad,
            abogadoAsignado,
            estado,
            creado: Date.now(),
            creador: {
                id: usuario.uid,
                nombre: usuario.displayName
            },
            numeroReasignaciones: 0,
            rechazada: false,
            nombreAbogadoAsignado
        }
        console.log('Oportunidad: ', oportunidad)

        try {
            // Guardar la oportunidad
            await firebase.db.collection('oportunidades').add(oportunidad);

        } catch (error) {
            console.log(error);
            guardarErrorDB(error)
        }
        try {
            await firebase.db.collection('contadorOportunidades').doc('OKxqPbwf4oBCh6NpfQUU').update({ contador: contadorDB + 1 })
        } catch (error) {
            console.log(error)
        }
        try {
            // Sumarle uno al contador del abogado asignado
            await firebase.db.collection('abogados').doc(abogadoAsignado).update({ contadorAsuntos: contadorAbogado + 1, ultimaOportunidad: Date.now() })
            emailjs.send('smtp_server', 'template_kZfiJX4o', template_params, 'user_xxs3Pu51cqPmbXqR7CsEW')
                .then((response) => {
                    console.log('SUCCESS!', response.status, response.text);
                }, (err) => {
                    console.log('FAILED...', err);
                });
            Swal.fire({
                // position: 'top-end',
                icon: 'success',
                title: 'La oportunidad se creó correctamente!',
                showConfirmButton: false,
                timer: 1200
            })
            setTimeout(() => {
                Router.push('/');
            }, 1200);
            console.log('todo OK')
        } catch (error) {
            console.log(error)
        }

    }
console.log('contadorDB', contadorDB)
    return (
        <div>
            <Layout>
                {error ? <PaginaError msg="Vaya... Parece que ha habido un error al editar el abogado, vuelve a intentarlo por favor"></PaginaError> : contadorDB === undefined ? <p>Cargando...</p> : (
                    usuario ? (
                        <>
                            <h1 className="titulo">Nueva Oportunidad</h1>
                            <form
                                onSubmit={handleSubmit}
                            >
                                <ContenedorFormularioLg>
                                    <fieldset className="mt-3">
                                        <legend className="pl-5">Rellena los Datos</legend>
                                        <div className="col-12 d-flex mt-2">
                                            <div className="col-4">
                                                <label htmlFor="idOportunidad">ID de Oportunidad</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="idOportunidad"
                                                    disabled
                                                    value={contadorDB !== undefined ? idOportunidad : ''}
                                                />
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="nombreCliente">Nombre del Cliente</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="nombreCliente"
                                                    onChange={handleChange}
                                                    value={nombreCliente}
                                                    placeholder="Nombre del cliente"
                                                />
                                                {errores.nombreCliente && <p className="alert alert-danger text-center p-2 mt-2">{errores.nombreCliente}</p>}

                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="telefonoCliente">Teléfono del Cliente</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="telefonoCliente"
                                                    onChange={handleChange}
                                                    value={telefonoCliente}
                                                    placeholder="Teléfono del cliente"
                                                />
                                                {errores.telefonoCliente && <p className="alert alert-danger text-center p-2 mt-2">{errores.telefonoCliente}</p>}
                                            </div>

                                        </div>

                                        <div className="col-12 d-flex abajo">
                                            <div className="col-4 mr-5">
                                                <label htmlFor="especialidad">Especialidad de la Oportunidad</label>
                                                <Select
                                                    name="especialidad"
                                                    options={especialidades}
                                                    onChange={handleChangeEspecialidad}
                                                    placeholder="Selecciona especialidad"
                                                />
                                                {errores.especialidad && <p className="alert alert-danger text-center p-2 mt-2">{errores.especialidad}</p>}
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="provinciaOportunidad">Provincia</label>
                                                <Select
                                                    name="provinciaOportunidad"
                                                    options={provincias}
                                                    onChange={handleChangeProvincia}
                                                    placeholder="Provincia de la oportunidad"
                                                />
                                                {errores.provinciaOportunidad && <p className="alert alert-danger text-center p-2 mt-2">{errores.provinciaOportunidad}</p>}
                                            </div>
                                        </div>
                                    </fieldset>

                                </ContenedorFormularioLg>

                                {abogadosSugeridos.length !== 0 ? (
                                    <>
                                        <h2>Socios Sugeridos</h2>
                                        <table className="table table-bordered table-hover w-100 ">

                                            <thead className="thead-dark text-center p-3">
                                                <tr className="m-2">
                                                    <th className="align-middle">Seleccionar Abogado</th>
                                                    <th className="align-middle">Nombre y Apellidos</th>
                                                    <th className="align-middle">Tipo</th>
                                                    <th className="align-middle">Especialidades</th>
                                                    <th className="align-middle">Dirección</th>
                                                    <th className="align-middle">Última Oportunidad</th>
                                                    <th className="align-middle">Asuntos Llevados</th>
                                                    <th className="align-middle">Asuntos Rechazados</th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                {abogadosSugeridos.map(abogado => {

                                                    return (
                                                        <SeleccionarAbogado
                                                            key={abogado.id}
                                                            abogado={abogado}
                                                            seleccionarRadio={seleccionarRadio}

                                                        />
                                                    )
                                                })}


                                            </tbody>

                                        </table>
                                    </>
                                ) :
                                    null

                                }

                                {/* Colaboradores */}
                                {colaboradoresSugeridos.length !== 0 ? (
                                    <>
                                        <h2 className="mt-4">Colaboradores Sugeridos</h2>
                                        <table className="table table-bordered table-hover w-100 ">

                                            <thead className="thead-dark text-center p-3">
                                                <tr className="m-2">
                                                    <th className="align-middle">Seleccionar Abogado</th>
                                                    <th className="align-middle">Nombre y Apellidos</th>
                                                    <th className="align-middle">Tipo</th>
                                                    <th className="align-middle">Especialidades</th>
                                                    <th className="align-middle">Dirección</th>
                                                    <th className="align-middle">Última Oportunidad</th>
                                                    <th className="align-middle">Asuntos Llevados</th>
                                                    <th className="align-middle">Asuntos Rechazados</th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                {colaboradoresSugeridos.map(abogado => {

                                                    return (
                                                        <SeleccionarAbogado
                                                            key={abogado.id}
                                                            abogado={abogado}
                                                            seleccionarRadio={seleccionarRadio}

                                                        />
                                                    )
                                                })}


                                            </tbody>

                                        </table>
                                    </>
                                ) :
                                    null

                                }
                                {sinSugerencias &&
                                    (
                                        <>
                                            <h2 className="text-center mb-3">No hay resultados de socios ni colaboradores en la provincia, elige otra opción:</h2>

                                            {/* Socios y Colaboradores de la CCAA */}
                                            <div className="d-flex justify-content-between mt-5">
                                                <h2 className="font-weight-bold">Comunidad Autonoma</h2>
                                                <div className="d-flex justify-content-between">
                                                    <InputTextOportunidades
                                                        type="text"
                                                        value={qComunidad}
                                                        onChange={e => guardarQComunidad(e.target.value)}
                                                        placeholder="Introduce el nombre del abogado..."
                                                    // className="mt-2"
                                                    />
                                                    <InputSubmitSugerencias disabled>Buscar</InputSubmitSugerencias>
                                                </div>


                                            </div><br />
                                            {/* <div className="d-flex mt-4 mb-4"> */}

                                            <h2>Socios</h2>
                                            <table className="table table-bordered table-hover w-100">

                                                <thead className="thead-dark text-center p-3">
                                                    <tr className="m-2">
                                                        <th className="align-middle">Seleccionar Abogado</th>
                                                        <th className="align-middle">Nombre y Apellidos</th>
                                                        <th className="align-middle">Tipo</th>
                                                        <th className="align-middle">Especialidades</th>
                                                        <th className="align-middle">Dirección</th>
                                                        <th className="align-middle">Última Oportunidad</th>
                                                        <th className="align-middle">Asuntos Llevados</th>
                                                        <th className="align-middle">Asuntos Rechazados</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    {sociosComunidad.length === 0 ? <SinResultados className="text-center" ><td colSpan="8">La búsqueda no dió resultados</td></SinResultados> : (


                                                        sociosComunidad.map(abogado => {

                                                            return (
                                                                <SeleccionarAbogado
                                                                    key={abogado.id}
                                                                    abogado={abogado}
                                                                    seleccionarRadio={seleccionarRadio}
                                                                />
                                                            )
                                                        })

                                                    )
                                                    }

                                                </tbody>

                                            </table>


                                            <h2>Colaboradores</h2>
                                            <table className="table table-bordered table-hover w-100">

                                                <thead className="thead-dark text-center p-3">
                                                    <tr className="m-2">
                                                        <th className="align-middle">Seleccionar Abogado</th>
                                                        <th className="align-middle">Nombre y Apellidos</th>
                                                        <th className="align-middle">Tipo</th>
                                                        <th className="align-middle">Especialidades</th>
                                                        <th className="align-middle">Dirección</th>
                                                        <th className="align-middle">Última Oportunidad</th>
                                                        <th className="align-middle">Asuntos Llevados</th>
                                                        <th className="align-middle">Asuntos Rechazados</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    {colaboradoresComunidad.length === 0 ? <SinResultados className="text-center" ><td colSpan="8">La búsqueda no dió resultados</td></SinResultados> : (


                                                        colaboradoresComunidad.map(abogado => {

                                                            return (
                                                                <SeleccionarAbogado
                                                                    key={abogado.id}
                                                                    abogado={abogado}
                                                                    seleccionarRadio={seleccionarRadio}
                                                                />
                                                            )
                                                        })

                                                    )
                                                    }

                                                </tbody>

                                            </table>


                                            {/* Socios y Colaboradores Nacionales */}
                                            <div className="d-flex justify-content-between mt-5">
                                                <h2 className="font-weight-bold">Nivel Nacional</h2>
                                                <div className="d-flex justify-content-between">
                                                    <InputTextOportunidades
                                                        type="text"
                                                        value={q}
                                                        onChange={e => guardarQ(e.target.value)}
                                                        placeholder="Introduce el nombre del abogado..."
                                                    // className="mt-2"
                                                    />
                                                    <InputSubmitSugerencias disabled>Buscar</InputSubmitSugerencias>
                                                </div>

                                            </div><br />

                                            {/* Tabla Socios NACIONALES */}
                                            <h2>Socios</h2>
                                            <table className="table table-bordered table-hover w-100">

                                                <thead className="thead-dark text-center p-3">
                                                    <tr className="m-2">
                                                        <th className="align-middle">Seleccionar Abogado</th>
                                                        <th className="align-middle">Nombre y Apellidos</th>
                                                        <th className="align-middle">Tipo</th>
                                                        <th className="align-middle">Especialidades</th>
                                                        <th className="align-middle">Dirección</th>
                                                        <th className="align-middle">Última Oportunidad</th>
                                                        <th className="align-middle">Asuntos Llevados</th>
                                                        <th className="align-middle">Asuntos Rechazados</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    {sociosNacionales.length === 0 ? <SinResultados className="text-center" ><td colSpan="8">La búsqueda no dió resultados</td></SinResultados> : (


                                                        sociosNacionales.map(abogado => {

                                                            return (
                                                                <SeleccionarAbogado
                                                                    key={abogado.id}
                                                                    abogado={abogado}
                                                                    seleccionarRadio={seleccionarRadio}
                                                                />
                                                            )
                                                        })

                                                    )
                                                    }

                                                </tbody>

                                            </table>

                                            {/* Tabla Colaboradores NACIONALES */}
                                            <h2>Colaboradores</h2>
                                            <table className="table table-bordered table-hover w-100">

                                                <thead className="thead-dark text-center p-3">
                                                    <tr className="m-2">
                                                        <th className="align-middle">Seleccionar Abogado</th>
                                                        <th className="align-middle">Nombre y Apellidos</th>
                                                        <th className="align-middle">Tipo</th>
                                                        <th className="align-middle">Especialidades</th>
                                                        <th className="align-middle">Dirección</th>
                                                        <th className="align-middle">Última Oportunidad</th>
                                                        <th className="align-middle">Asuntos Llevados</th>
                                                        <th className="align-middle">Asuntos Rechazados</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    {colaboradoresNacionales.length === 0 ? <SinResultados className="text-center" ><td colSpan="8">La búsqueda no dió resultados</td></SinResultados> : (


                                                        colaboradoresNacionales.map(abogado => {

                                                            return (
                                                                <SeleccionarAbogado
                                                                    key={abogado.id}
                                                                    abogado={abogado}
                                                                    seleccionarRadio={seleccionarRadio}
                                                                />
                                                            )
                                                        })

                                                    )
                                                    }

                                                </tbody>

                                            </table>

                                        </>

                                    )}
                                {errores.abogadoAsignado ? (
                                    <p className="alert alert-danger text-center p-2 mt-2">{errores.abogadoAsignado}</p>
                                ) : null}
                                {error && <p className="alert alert-danger text-center p2 mt-2 mb-2">{error}</p>}
                                <div className="col-12 d-flex justify-content-center">
                                    <Link href="/">
                                        <BotonCancelar
                                            className="btn btn-secondary"
                                        >Cancelar</BotonCancelar>
                                    </Link>

                                    <BotonAceptar
                                        className="btn btn-primary"
                                        type="submit"
                                    >Crear Oportunidad</BotonAceptar>
                                </div>
                            </form>

                        </>
                    ) :
                        <>
                            <PaginaError msg={`Tienes que estar logueado para acceder a esta sección.`}></PaginaError>
                            <div className="text-center">
                                <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                            </div>
                        </>
                )}

            </Layout>
        </div>
    );
}

export default NuevaOportunidad;
