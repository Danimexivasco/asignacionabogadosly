import React, { useContext, useState } from 'react';
import Link from 'next/link';
import Select from 'react-select';
import Router from 'next/router';
import Swal from 'sweetalert2';

import PaginaError from '../components/layout/PaginaError';
import Layout from '../components/layout/Layout';
import { ContenedorFormularioLg, BotonCancelar, BotonAceptar } from '../components/ui/Formulario';
import {
    provincias,
    tipos,
    comunidades,
    especialidades
} from '../components/ui/OpcionesSelectores';

// Validaciones
import useValidacion from '../hooks/useValidacion';
import validarNuevoAbogado from '../validacion/validarNuevoAbogado';

import { FirebaseContext } from '../firebase/index';
import firebase from '../firebase/index';


const STATE_INICIAL = {
    tipo: '',
    nombre: '',
    apellidos: '',
    idLawyou: '',
    dni: '',
    calle: '',
    provincia: '',
    ciudad: '',
    comunidadAutonoma: '',
    cp: '',
    emailAbogado: '',
    especialidadesAbogado: [],
    dniDuplicado: false,
    contadorAsuntos: 0,
    contadorRechazados: 0,
    ultimaOportunidad: ''
}

const NuevoAbogado = () => {

    const [error, guardarError] = useState(false);

    const { valores, errores, handleSubmit, handleChange, handleChangeTipo, handleChangeProvincia, handleChangeComunidad, handleChangeEspecialidad } = useValidacion(STATE_INICIAL, validarNuevoAbogado, nuevoAbogado);


    // Destructuring de los valores
    const { tipo, nombre, apellidos, idLawyou, dni, calle, provincia, especialidadesAbogado, ciudad, comunidadAutonoma, cp, contadorAsuntos, emailAbogado, contadorRechazados, ultimaOportunidad } = valores;


    const { usuario } = useContext(FirebaseContext);


    async function nuevoAbogado() {
        if (!usuario) {
            return Router.push('/iniciar-sesion');
        }
        // const direccion = `${calle}, ${ciudad}, ${provincia}, ${comunidadAutonoma} - ${cp}`;

        // Creamos el objeto de abogado
        const abogado = {
            tipo,
            nombre,
            apellidos,
            idLawyou,
            dni,
            calle,
            provincia,
            ciudad,
            emailAbogado,
            direccion: `${calle}, ${ciudad}, ${provincia}, ${comunidadAutonoma} - ${cp}`,
            comunidadAutonoma,
            cp,
            especialidadesAbogado,
            creado: Date.now(),
            creador: {
                id: usuario.uid,
                nombre: usuario.displayName
            },
            contadorAsuntos,
            contadorRechazados,
            ultimaOportunidad
        };

        try {
            await firebase.db.collection('abogados').add(abogado);
            Swal.fire({
                // position: 'top-end',
                icon: 'success',
                title: 'El abogado se creó correctamente!',
                showConfirmButton: false,
                timer: 1200
            })
            setTimeout(() => {
                Router.push('/abogados');
            }, 1200);
        } catch (error) {
            console.log(error);
            guardarError(error);
        }
    };

    return (
        <div>
            <Layout>
                {usuario ? (
                    <>
                        <h1 className="titulo">Nuevo Abogado</h1>
                        <form
                            onSubmit={handleSubmit}
                        >
                            <ContenedorFormularioLg>
                                <fieldset>
                                    <legend>Datos Personales</legend>
                                    <div className="col-12 d-flex mt-3">
                                        <div className="col-4">
                                            <label htmlFor="tipo">Tipo</label>
                                            <Select
                                                onChange={handleChangeTipo}
                                                options={tipos}
                                                name="tipo"
                                                placeholder="Selecciona Tipo"
                                            />
                                            {errores.tipo && <p className="alert alert-danger text-center p-2 mt-2">{errores.tipo}</p>}
                                        </div>
                                        <div className="col-4">
                                            <label htmlFor="idLawyou">ID Lawyou</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="idLawyou"
                                                onChange={handleChange}
                                                value={idLawyou}
                                                placeholder="Ej. A050"
                                            />
                                        </div>
                                        <div className="col-4">
                                            <label htmlFor="dni">DNI</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="dni"
                                                onChange={handleChange}
                                                value={dni}
                                                placeholder="DNI del Abogado"
                                            />
                                            <small>Ej. 12345678A</small>
                                            {errores.dni && <p className="alert alert-danger text-center p-2 mt-2">{errores.dni}</p>}
                                            {errores.dniDuplicado && <p className="alert alert-danger text-center p-2 mt-2">{errores.dniDuplicado}</p>}
                                        </div>

                                    </div>

                                    <div className="col-12 d-flex abajo">
                                        <div className="col-4">
                                            <label htmlFor="nombre">Nombre</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="nombre"
                                                onChange={handleChange}
                                                value={nombre}
                                                placeholder="Nombre del abogado"
                                            />
                                            {errores.nombre && <p className="alert alert-danger text-center p-2 mt-2">{errores.nombre}</p>}
                                        </div>
                                        <div className="col-4">
                                            <label htmlFor="apellidos">Apellidos</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="apellidos"
                                                onChange={handleChange}
                                                value={apellidos}
                                                placeholder="Apellidos del abogado"
                                            />
                                            {errores.apellidos && <p className="alert alert-danger text-center p-2 mt-2">{errores.apellidos}</p>}
                                        </div>
                                        <div className="col-4">
                                            <label htmlFor="especialidades">Especialidades</label>
                                            <Select
                                                name="especialidades"
                                                options={especialidades}
                                                isMulti
                                                className="basic-multi-select"
                                                classNamePrefix="select"
                                                onChange={handleChangeEspecialidad}
                                                placeholder="Selecciona Especialidades"
                                            />
                                            {errores.especialidades && <p className="alert alert-danger text-center p-2 mt-2">{errores.especialidades}</p>}
                                        </div>

                                    </div>

                                    <div className="col-12 d-flex abajo">
                                        <div className="col-4">
                                            <label htmlFor="emailAbogado">Email</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="emailAbogado"
                                                onChange={handleChange}
                                                value={emailAbogado}
                                                placeholder="Email"
                                            />
                                            {errores.emailAbogado && <p className="alert alert-danger text-center p-2 mt-2">{errores.emailAbogado}</p>}
                                        </div>
                                    </div>




                                </fieldset>

                                <fieldset className="mt-5">
                                    <legend>Datos de Facturación</legend>
                                    <div className="col-12 d-flex mt-2 justify-content-between">
                                        <div className="col-6">
                                            <label htmlFor="calle">Calle</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="calle"
                                                onChange={handleChange}
                                                value={calle}
                                                placeholder="Ej. Calle de Miramon Nº9, 3-A"
                                            />
                                            {errores.calle && <p className="alert alert-danger text-center p-2 mt-2">{errores.calle}</p>}
                                        </div>
                                        <div className="col-6">
                                            <label htmlFor="ciudad">Ciudad</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="ciudad"
                                                onChange={handleChange}
                                                value={ciudad}
                                                placeholder="Ej. Sevilla"
                                            />
                                            {errores.ciudad && <p className="alert alert-danger text-center p-2 mt-2">{errores.ciudad}</p>}
                                        </div>
                                    </div>

                                    <div className="col-12 d-flex  abajo">
                                        <div className="col-4">
                                            <label htmlFor="provincia">Provincia</label>
                                            <Select
                                                onChange={handleChangeProvincia}
                                                options={provincias}
                                                name="provincia"
                                                placeholder="Selecciona Provincia"
                                            />
                                            {errores.provincia && <p className="alert alert-danger text-center p-2 mt-2">{errores.provincia}</p>}
                                        </div>
                                        <div className="col-4">
                                            <label htmlFor="comunidadAutonoma">Comunidad Autónoma</label>
                                            <Select
                                                onChange={handleChangeComunidad}
                                                options={comunidades}
                                                name="comunidadAutonoma"
                                                placeholder="Selecciona Comunidad Autonoma"
                                            />
                                            {errores.comunidadAutonoma && <p className="alert alert-danger text-center p-2 mt-2">{errores.comunidadAutonoma}</p>}
                                        </div>
                                        <div className="col-4">
                                            <label htmlFor="cp">Código Postal</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="cp"
                                                onChange={handleChange}
                                                value={cp}
                                                placeholder="Ej. 20532"
                                            />
                                            {errores.cp && <p className="alert alert-danger text-center p-2 mt-2">{errores.cp}</p>}
                                        </div>
                                    </div>
                                </fieldset>

                            </ContenedorFormularioLg>

                            <div className="col-12 d-flex justify-content-center">
                                {error && <p className="alert alert-danger text-center p2 mt-2 mb-2">{error}</p>}
                                <Link href="/abogados">
                                    <BotonCancelar
                                        className="btn btn-secondary"
                                    >Cancelar</BotonCancelar>
                                </Link>

                                <BotonAceptar
                                    className="btn btn-primary"
                                    type="submit"
                                >Crear</BotonAceptar>
                            </div>
                        </form>

                    </>
                ) :
                    <>
                        <PaginaError msg={`Tienes que estar logueado para acceder a esta sección.`}></PaginaError>
                        <div className="text-center">
                            <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                        </div>
                    </>
                }

            </Layout>
        </div>
    )
}

export default NuevoAbogado;

