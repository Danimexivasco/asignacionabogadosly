import React, { useContext, useState, useEffect } from 'react';
// import { useRouter } from 'next/router';
import Router, { useRouter } from 'next/router';
import Link from 'next/link';
import Select from 'react-select';
import Swal from 'sweetalert2';
import emailjs from 'emailjs-com';

import Layout from '../../components/layout/Layout';
import { ContenedorFormularioLg, BotonAceptar, BotonCancelar } from '../../components/ui/Formulario';
import PaginaError from '../../components/layout/PaginaError';
import {
    provincias,
    comunidades,
    especialidades,
    estados,
    motivosRechazo
} from '../../components/ui/OpcionesSelectores';
import useAbogadosSugerencia from '../../hooks/useAbogadosSugerencia';
import { SinResultados } from '../../components/ui/SinResultados';
import SeleccionarAbogado from '../../components/layout/SeleccionarAbogado';
import { InputText, InputSubmit } from '../../components/ui/Busqueda';
import { BotonEliminar } from '../../components/ui/BotonEliminar';
import useContadorOportunidades from '../../hooks/useContadorOportunidades';

// Validaciones
import useValidacionOportunidadesEditar from '../../hooks/useValidacionOportunidadesEditar';
import validarEditarOportunidad from '../../validacion/validarEditarOportunidad';

// Firebase
import { FirebaseContext } from '../../firebase/index';


const Oportunidad = () => {

    // Routing para obtener el ID actual
    const router = useRouter();
    const { query: { id } } = router;

    const [error, guardarError] = useState(false);
    const [errorDB, guardarErrorDB] = useState([]);
    const [contadorAbogado, guardarContadorAbogado] = useState(0);
    const [otrasSugerencias, guardarOtrasSugerencias] = useState([]);
    const [contadorDB, guardarContadorDB] = useState(0);
    const [consultarDB, guardarConsultarDB] = useState(true)
    const [oportunidad, guardarOportunidad] = useState({});
    const [abogadoAsignadoOriginal, guardarAbogadoAsignadoOriginal] = useState('');
    const [nombreAbogado, guardarNombreAbogado] = useState('');
    const [emailAbogadoAsignado, guardarEmailAbogadoAsignado] = useState('');
    const [tipoUsuario, guardarTipoUsuario] = useState('');

    const { firebase, usuario } = useContext(FirebaseContext);



    // Funcion para buscar al usuario
    const buscarUsuario = async () => {
        await firebase.db.collection("usuarios").where("email", "==", usuario.email).onSnapshot(manejarSnapShotUsuario);
    }

    function manejarSnapShotUsuario(snapshot) {
        const usuarioBD = snapshot.docs.map(doc => {
            return {
                id: doc.id,
                ...doc.data()
            }
        })
        if (usuarioBD[0] !== undefined) {
            guardarTipoUsuario(usuarioBD[0].rol)
            console.log(usuarioBD[0].rol)
        }
    }

    useEffect(() => {
        let montado = true;
        if (usuario !== undefined && usuario !== null) {
            buscarUsuario();
        }
        return () => montado = false;
    }, [usuario])




    useEffect(() => {
        // Declaramos variable para trackear
        let estaCancelado = false;
        // console.log('useEffect [ID].js')

        if (id && consultarDB) {
            const obtenerOportunidad = async () => {
                const oportunidadQuery = await firebase.db.collection('oportunidades').doc(id);
                const oportunidad = await oportunidadQuery.get();

                // Se ejecuta solo si esta montado
                if (!estaCancelado) {
                    if (oportunidad.exists) {
                        guardarOportunidad(oportunidad.data());
                    } else {
                        guardarError(true);
                    }
                    guardarConsultarDB(false);
                }

            }
            obtenerOportunidad();
        }
        return () => {
            // Ponemos la variable a true para evitar que intente actualizar un state inexistente
            estaCancelado = true;
        }
    }, [id, oportunidad])

    // FILTRADO
    const [abogadosSugeridos, guardarAbogadosSugeridos] = useState([]);
    const [colaboradoresSugeridos, guardarColaboradoresSugeridos] = useState([]);
    const [sinSugerencias, guardarSinSugerencias] = useState(false);
    const [busquedaManual, guardarBusquedaManual] = useState(false);
    const [datosAbogado, guardarDatosAbogado] = useState(0);



    const { abogados } = useAbogadosSugerencia('contadorAsuntos');
    const { contador } = useContadorOportunidades();


    const { valores, errores, handleSubmit, handleChange, handleChangeProvincia, handleChangeComunidad, handleChangeEspecialidad, handleChangeAbogadoAsignado, handleChangeEstado, handleChangeMotivo } = useValidacionOportunidadesEditar(oportunidad, validarEditarOportunidad, editarOportunidad);
    const { nombreCliente, telefonoCliente, especialidad, idOportunidad, provinciaOportunidad, abogadoAsignado, estado, creado, creador, numeroReasignaciones, idOportunidadOriginal, rechazada, reasignada, abogadosRechazadores, nombreAbogadoAsignado, motivoRechazo, textoMotivoRechazo } = valores;


    const valorProvincia = provincias.find(prov => prov.value === provinciaOportunidad);
    const valorEspecialidad = especialidades.filter(espec => espec.value === especialidad);
    const valorEstado = estados.filter(est => est.value === estado);
    const valorMotivoRechazo = (motivoRechazo ? motivosRechazo.filter(mot => mot.value === motivoRechazo) : "");
    // console.log("valorMotivoRechazo",valorMotivoRechazo )


    useEffect(() => {
        if (contador !== undefined) {
            guardarContadorDB(contador.contador);
            console.log('contadorDB', contador.contador)
        }
    }, [contador])

    useEffect(() => {
        let montado = true;
        console.log(montado)
        if (Object.keys(oportunidad).length !== 0) {
            if (montado) {
                guardarAbogadoAsignadoOriginal(abogadoAsignado)
                console.log('ABOGADO ORIGINAL: ', oportunidad.abogadoAsignado)
            }
        }
        return () => montado = false;
    }, [oportunidad])

    // Sugerencias de SOCIOS y Colaboradores
    useEffect(() => {
        if (Object.keys(valores).length !== 0 && abogados) {
            console.log('VALORES: ', valores)
            const provinciaMinuscula = provinciaOportunidad.toLowerCase();
            const provinciaSinTildes = quitarTildes(provinciaMinuscula);
            const abogadosFiltrados = abogados.filter(abogado => {
                if (especialidad !== '' && provinciaOportunidad !== '') {
                    return (
                        quitarTildes(abogado.provincia.toLowerCase()).includes(provinciaSinTildes) && abogado.especialidadesAbogado.includes(especialidad) && abogado.tipo !== 'Colaborador'
                    )
                }
            })
            const colaboradoresFiltrados = abogados.filter(abogado => {
                if (especialidad !== '' && provinciaOportunidad !== '') {
                    return (
                        quitarTildes(abogado.provincia.toLowerCase()).includes(provinciaSinTildes) && abogado.especialidadesAbogado.includes(especialidad) && abogado.tipo === 'Colaborador'
                    )
                }
            })
            guardarAbogadosSugeridos(abogadosFiltrados)
            console.log('sociosSugeridos', abogadosFiltrados)
            guardarColaboradoresSugeridos(colaboradoresFiltrados)
            console.log('colaboradoresSugeridos', colaboradoresFiltrados)
            if (especialidad !== '' && provinciaOportunidad !== '' && abogadosFiltrados.length === 0 && colaboradoresFiltrados.length === 0) {
                guardarSinSugerencias(true);
                guardarBusquedaManual(true);
                return;
            }
            guardarSinSugerencias(false);
            guardarBusquedaManual(false);
        }
    }, [provinciaOportunidad, especialidad])


    async function verContador() {
        if (abogadoAsignado) {
            const abogadoSeleccionado = await firebase.db.collection('abogados').doc(abogadoAsignado).get()
            guardarContadorAbogado(abogadoSeleccionado.data().contadorAsuntos);
            // guardarNombreAbogado(`${abogadoSeleccionado.data().nombre} ${abogadoSeleccionado.data().apellidos}`)
            guardarNombreAbogado(`${abogadoSeleccionado.data().nombre} ${abogadoSeleccionado.data().apellidos}`);
            guardarEmailAbogadoAsignado(abogadoSeleccionado.data().emailAbogado);
        }
    }

    useEffect(() => {
        verContador();
    }, [abogadoAsignado]);

    const quitarTildes = (palabra) => {
        const letras = { 'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u' };
        palabra = palabra.replace(/[áéíóú]/g, m => letras[m]);
        return palabra;
    }

    // Funcion para checar el abogado seleccionado
    const seleccionarRadio = (opcion) => {
        handleChangeAbogadoAsignado(opcion);
    }

    // Funcion de filtrado manual
    // FILTRADO
    const [q, guardarQ] = useState('');
    const [resultados, guardarResultados] = useState([])


    useEffect(() => {
        if (otrasSugerencias) {
            // console.log("entró: ", otrasSugerencias)
            const busqueda = q.toLowerCase();
            const busquedaSinTildes = quitarTildes(busqueda);

            const abogadosFiltrados = otrasSugerencias.filter(abogado => {
                return (
                    `${quitarTildes(abogado.nombre.toLowerCase())} ${quitarTildes(abogado.apellidos.toLowerCase())}`.includes(busquedaSinTildes)
                    // quitarTildes(abogado.nombre.toLowerCase()).includes(busquedaSinTildes) ||
                    // quitarTildes(abogado.apellidos.toLowerCase()).includes(busquedaSinTildes)
                )
            })
            guardarResultados(abogadosFiltrados)
            console.log('abogadosFiltrador especialidad', abogadosFiltrados)
        }

    }, [q, otrasSugerencias])


    const resultadosProvincia = async () => {
        if (Object.keys(valores).length !== 0) {
            await firebase.db.collection('abogados').where("especialidadesAbogado", "array-contains", especialidad).onSnapshot(manejarSnapShot);
        }
    }

    useEffect(() => {
        if (valores) {
            resultadosProvincia();
        }
    }, [especialidad])

    function manejarSnapShot(snapshot) {
        const abogadosDB = snapshot.docs.map(doc => {
            return {
                id: doc.id,
                ...doc.data()
            }
        })
        const abogadosOrdenados = abogadosDB.sort((a, b) => (a.contadorAsuntos > b.contadorAsuntos) ? 1 : -1)
        guardarOtrasSugerencias(abogadosOrdenados);
    }

    const contadorRechazadosAbogado = async () => {
        if (abogadoAsignado !== '') {
            const contadorRechazados = await firebase.db.collection('abogados').doc(abogadoAsignado).get()
            console.log(contadorRechazados.data().contadorRechazados)
            guardarDatosAbogado(contadorRechazados.data().contadorRechazados);

        }
    }

    useEffect(() => {
        if (Object.keys(oportunidad).length !== 0 && (abogadoAsignado !== undefined)) {
            contadorRechazadosAbogado()
        }
    }, [abogadoAsignado, oportunidad])


    const template_params = {
        "reply_to": "servicios.lawyou@lawyoulegal.com",
        "emailAbogado": emailAbogadoAsignado,
        "destinatario": nombreAbogadoAsignado,
        "from_name": "Lawyou",
        "mensaje_html": `<ul>
                            <li><b>Nombre Cliente</b>:  ${nombreCliente}</li>
                            <li><b>Telefono</b>:  ${telefonoCliente}</li>
                            <li><b>Especialidad</b>:  ${especialidad}</li>
                            <li><b>Provincia</b>:  ${provinciaOportunidad}</li>
                        </ul><br>
                        <p>En caso de no haber confirmado tu disponibilidad, no olvides hacerlo llamando al 667 60 66 11 o respondiendo a este mail</p>`
    }

    async function editarOportunidad() {
        if (!usuario) {
            return Router.push('/iniciar-sesion');
        }

        const oportunidad = {
            idOportunidad,
            nombreCliente,
            telefonoCliente,
            especialidad,
            provinciaOportunidad,
            abogadoAsignado,
            estado,
            creado,
            creador,
            actualizado: Date.now(),
            actualizaUsuario: {
                id: usuario.uid,
                nombre: usuario.displayName
            },
            nombreAbogadoAsignado: nombreAbogado
        }

        if (abogadoAsignadoOriginal !== abogadoAsignado && abogadoAsignadoOriginal !== '') {
            console.log('ORIGInAL', abogadoAsignadoOriginal)
            Swal.fire({
                icon: 'error',
                title: 'No se puede cambiar el abogado asignado directamente',
                text: 'Para ello, habría que rechazar la oportunidad y reasignarsela al abogado deseado',
                showConfirmButton: true,
            })
        } else if (estado !== "Rechazada") {
            if (!usuario) {
                return router.push("/login");
            }


            // Updatear en firebase
            try {
                if (numeroReasignaciones >= 1) {
                    await firebase.db.collection('oportunidades').doc(id).update({ ...oportunidad, reasignada: true })
                    console.log("reasignada TRUE", oportunidad)
                } else {
                    await firebase.db.collection('oportunidades').doc(id).update(oportunidad)
                    console.log("Editando oportunidad...", oportunidad)
                }
            } catch (error) {
                console.log(error)
                guardarErrorDB({
                    ...errorDB,
                    error
                })
            }
            if (!reasignada) {
                try {
                    // Sumarle uno al contador del abogado asignado
                    await firebase.db.collection('abogados').doc(abogadoAsignado).update({ contadorAsuntos: contadorAbogado + 1, ultimaOportunidad: Date.now() });
                    emailjs.send('smtp_server', 'template_kZfiJX4o', template_params, 'user_xxs3Pu51cqPmbXqR7CsEW')
                        .then((response) => {
                            console.log('SUCCESS!', response.status, response.text);
                        }, (err) => {
                            console.log('FAILED...', err);
                        });
                } catch (error) {
                    console.log(error)
                    guardarErrorDB({
                        ...errorDB,
                        error
                    })
                }
            }
            Swal.fire({
                icon: 'success',
                title: 'La oportunidad se actualizó correctamente!',
                showConfirmButton: false,
                timer: 1200
            })
            router.push("/")

        } else {
            //Cuando se RECHAZAN
            if (!usuario) {
                return router.push("/login");
            }


            // Updatear firebase OPORTUNIDAD 
            try {
                if (textoMotivoRechazo !== undefined) {
                    await firebase.db.collection('oportunidades').doc(id).update({ ...oportunidad, rechazada: true, motivoRechazo: motivoRechazo, textoMotivoRechazo: textoMotivoRechazo })

                } else {
                    await firebase.db.collection('oportunidades').doc(id).update({ ...oportunidad, rechazada: true, motivoRechazo: motivoRechazo })
                }
                console.log("Editando oportunidad...", oportunidad)
            } catch (error) {
                console.log(error)
                guardarErrorDB({
                    ...errorDB,
                    error
                })
            }

            // Updatear firebase ABOGADO, Hay que sumarle una a la lista de rechazadas al abogado
            try {
                await firebase.db.collection('abogados').doc(abogadoAsignado).update({ contadorRechazados: datosAbogado + 1 })
            } catch (error) {
                console.log(error)
            }

            // Crear una oportunidad "gemela" con la coletilla -R + crear numeroReasignaciones  idOportunidad: idOportunidad + numeroReasignaciones.toString()
            // la oportunidad debe de tener el estado pendiente de asignar
            const numeroReasignacionesActual = numeroReasignaciones + 1
            console.log('numeroReasignaciones', numeroReasignaciones)
            console.log('numeroReasignacionesActual: ', numeroReasignacionesActual)

            if (numeroReasignaciones <= 0) {
                let abogadosRechazadores = []
                abogadosRechazadores.push(abogadoAsignado)
                console.log('abogados RECHAZADORES: ', abogadosRechazadores)
                const oportunidadClonada = {
                    idOportunidadOriginal: idOportunidad,
                    idOportunidad: `${idOportunidad}-R${numeroReasignacionesActual}`,
                    nombreCliente,
                    telefonoCliente,
                    especialidad,
                    provinciaOportunidad,
                    abogadoAsignado: '',
                    estado: "Pendiente Asignar",
                    creado: Date.now(),
                    creador: {
                        id: usuario.uid,
                        nombre: usuario.displayName
                    },
                    numeroReasignaciones: numeroReasignacionesActual,
                    reasignada: false,
                    abogadosRechazadores,
                    nombreAbogadoAsignado
                }
                try {
                    await firebase.db.collection('oportunidades').add(oportunidadClonada)
                    console.log('oportunidadClonada', oportunidadClonada)

                } catch (error) {
                    console.log(error)
                }
            } else {
                abogadosRechazadores.push(abogadoAsignado)
                console.log('abogados RECHAZADORES: ', abogadosRechazadores)
                const oportunidadClonada = {
                    idOportunidadOriginal,
                    idOportunidad: `${idOportunidadOriginal}-R${numeroReasignacionesActual}`,
                    nombreCliente,
                    telefonoCliente,
                    especialidad,
                    provinciaOportunidad,
                    abogadoAsignado: '',
                    estado: "Pendiente Asignar",
                    creado: Date.now(),
                    creador: {
                        id: usuario.uid,
                        nombre: usuario.displayName
                    },
                    numeroReasignaciones: numeroReasignacionesActual,
                    reasignada: false,
                    abogadosRechazadores,
                    nombreAbogadoAsignado: ''
                }
                try {
                    await firebase.db.collection('oportunidades').add(oportunidadClonada)
                    console.log('oportunidadClonada', oportunidadClonada)

                } catch (error) {
                    console.log(error)
                }
            }

            console.log('Rechazando..  TODO OK')

            // Tras esto, redireccionar y mostrar mensaje
            router.push('/');
        }
    }

    const eliminarOportunidad = async () => {
        if (!usuario) {
            return router.push("/login");
        }

        // Modal de SWAL
        Swal.fire({
            title: '¿Estás seguro?',
            text: "No podrás deshacer esta acción!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                try {
                    firebase.db.collection('oportunidades').doc(idOportunidadOriginal).delete();
                    Swal.fire({
                        icon: 'success',
                        title: 'La Oportunidad ha sido eliminada!',
                        showConfirmButton: false,
                        timer: 1200
                    })
                    setTimeout(() => {
                        router.push("/")
                    }, 1200);
                } catch (error) {
                    console.log(error)
                }

            }
        })

    }

    return (
        <div>
            <Layout>
                {error ? <PaginaError msg="Vaya... Parece que ha habido un error al editar el abogado, vuelve a intentarlo por favor"></PaginaError> : Object.keys(valores).length === 0 ? <p>Cargando...</p> : (
                    usuario ? (
                        <>

                            {tipoUsuario === "superAdmin" ? (
                                <div className="col-12 d-flex justify-content-between">
                                    <h1 className="titulo">Editar Oportunidad</h1>
                                    <BotonEliminar
                                        className="col-2 btn btn-danger"
                                    onClick={()=>eliminarOportunidad()}
                                    >Eliminar Oportunidad</BotonEliminar>
                                </div>
                            ) : (
                                    <h1 className="titulo">Editar Oportunidad</h1>
                                )}


                            <form
                                onSubmit={handleSubmit}
                            >
                                <ContenedorFormularioLg>
                                    <fieldset className="mt-5">
                                        <legend className="pl-5">Verifica los Datos</legend>
                                        <div className="col-12 d-flex mt-2">
                                            <div className="col-4">
                                                <label htmlFor="idOportunidad">ID de Oportunidad</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="idOportunidad"
                                                    disabled
                                                    defaultValue={idOportunidad}
                                                />
                                                {/* {errores.nombreCliente && <p className="alert alert-danger text-center p-2 mt-2">{errores.nombreCliente}</p>} */}
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="nombreCliente">Nombre del Cliente</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="nombreCliente"
                                                    onChange={handleChange}
                                                    placeholder="Nombre del cliente"
                                                    defaultValue={nombreCliente}
                                                />
                                                {errores.nombreCliente && <p className="alert alert-danger text-center p-2 mt-2">{errores.nombreCliente}</p>}

                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="telefonoCliente">Teléfono del Cliente</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="telefonoCliente"
                                                    onChange={handleChange}
                                                    placeholder="Teléfono del cliente"
                                                    defaultValue={telefonoCliente}
                                                />
                                                {errores.telefonoCliente && <p className="alert alert-danger text-center p-2 mt-2">{errores.telefonoCliente}</p>}
                                            </div>

                                        </div>

                                        <div className="col-12 d-flex abajo">
                                            <div className="col-4">
                                                <label htmlFor="especialidad">Especialidad de la Oportunidad</label>
                                                <Select
                                                    name="especialidad"
                                                    options={especialidades}
                                                    onChange={handleChangeEspecialidad}
                                                    placeholder="Selecciona especialidad"
                                                    defaultValue={valorEspecialidad}
                                                />
                                                {errores.especialidad && <p className="alert alert-danger text-center p-2 mt-2">{errores.especialidad}</p>}
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="provinciaOportunidad">Provincia</label>
                                                <Select
                                                    name="provinciaOportunidad"
                                                    options={provincias}
                                                    onChange={handleChangeProvincia}
                                                    placeholder="Provincia de la oportunidad"
                                                    defaultValue={valorProvincia}
                                                />
                                                {errores.provinciaOportunidad && <p className="alert alert-danger text-center p-2 mt-2">{errores.provinciaOportunidad}</p>}
                                            </div>
                                            <div className="col-4">
                                                <label htmlFor="estado">Estado de la oportunidad</label>
                                                <Select
                                                    name="estado"
                                                    options={estados}
                                                    onChange={handleChangeEstado}
                                                    isDisabled={rechazada ? true : false}
                                                    placeholder="Estado de la Oportunidad"
                                                    defaultValue={valorEstado}
                                                />
                                                {/* {errores.provinciaOportunidad && <p className="alert alert-danger text-center p-2 mt-2">{errores.provinciaOportunidad}</p>} */}
                                            </div>
                                        </div>
                                        {estado === 'Rechazada' && (
                                            <div className="col-12">
                                                <div className="col-4">
                                                    <label htmlFor="motivoRechazo">Motivo del Rechazo:</label>
                                                    <Select
                                                        name="motivoRechazo"
                                                        options={motivosRechazo}
                                                        onChange={handleChangeMotivo}
                                                        isDisabled={rechazada ? true : false}
                                                        placeholder="Motivo del rechazo"
                                                        defaultValue={valorMotivoRechazo}
                                                    />
                                                </div>
                                            </div>)}
                                        {motivoRechazo === "Otro" && (
                                            <div className="col-12 mt-4">
                                                <div className="col-12">
                                                    <label htmlFor="textoMotivoRechazo">Explica el motivo:</label>
                                                    {rechazada ? (
                                                        <textarea
                                                            className="form-control"
                                                            id="textoMotivoRechazo"
                                                            name="textoMotivoRechazo"
                                                            onChange={handleChange}
                                                            defaultValue={textoMotivoRechazo ? textoMotivoRechazo : null}
                                                            disabled
                                                        ></textarea>

                                                    ) : (
                                                            <textarea
                                                                className="form-control"
                                                                id="textoMotivoRechazo"
                                                                name="textoMotivoRechazo"
                                                                onChange={handleChange}
                                                                defaultValue={textoMotivoRechazo ? textoMotivoRechazo : null}
                                                            ></textarea>
                                                        )}

                                                </div>
                                            </div>
                                        )}


                                    </fieldset>

                                </ContenedorFormularioLg>

                                {abogadosSugeridos.length !== 0 ? (
                                    <>
                                        <h2>Socios Sugeridos</h2>
                                        <table className="table table-bordered table-hover w-100 ">

                                            <thead className="thead-dark text-center p-3">
                                                <tr className="m-2">
                                                    <th className="align-middle">Seleccionar Abogado</th>
                                                    <th className="align-middle">Nombre y Apellidos</th>
                                                    <th className="align-middle">Tipo</th>
                                                    <th className="align-middle">Especialidades</th>
                                                    <th className="align-middle">Dirección</th>
                                                    <th className="align-middle">Última Oportunidad</th>
                                                    <th className="align-middle">Asuntos Llevados</th>
                                                    <th className="align-middle">Asuntos Rechazados</th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                {abogadosSugeridos.map(abogado => {

                                                    return (
                                                        <SeleccionarAbogado
                                                            key={abogado.id}
                                                            abogado={abogado}
                                                            seleccionarRadio={seleccionarRadio}
                                                            abogadoAsignado={abogadoAsignado}
                                                            abogadosRechazadores={abogadosRechazadores}

                                                        />
                                                    )
                                                })}


                                            </tbody>

                                        </table>
                                    </>
                                ) :
                                    null

                                }

                                {/* Colaboradores */}
                                {colaboradoresSugeridos.length !== 0 ? (
                                    <>
                                        <h2 className="mt-4">Colaboradores Sugeridos</h2>
                                        <table className="table table-bordered table-hover w-100 ">

                                            <thead className="thead-dark text-center p-3">
                                                <tr className="m-2">
                                                    <th className="align-middle">Seleccionar Abogado</th>
                                                    <th className="align-middle">Nombre y Apellidos</th>
                                                    <th className="align-middle">Tipo</th>
                                                    <th className="align-middle">Especialidades</th>
                                                    <th className="align-middle">Dirección</th>
                                                    <th className="align-middle">Última Oportunidad</th>
                                                    <th className="align-middle">Asuntos Llevados</th>
                                                    <th className="align-middle">Asuntos Rechazados</th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                {colaboradoresSugeridos.map(abogado => {

                                                    return (
                                                        <SeleccionarAbogado
                                                            key={abogado.id}
                                                            abogado={abogado}
                                                            seleccionarRadio={seleccionarRadio}
                                                            abogadoAsignado={abogadoAsignado}
                                                            abogadosRechazadores={abogadosRechazadores}
                                                        />
                                                    )
                                                })}


                                            </tbody>

                                        </table>
                                    </>
                                ) :
                                    null

                                }
                                {sinSugerencias &&
                                    (
                                        <>
                                            <h2 className="text-center mb-3">No hay resultados de socios ni colaboradores en la provincia, elige otra opción:</h2>


                                            <div className="d-flex mt-4 mb-4">
                                                <InputText
                                                    type="text"
                                                    value={q}
                                                    onChange={e => guardarQ(e.target.value)}
                                                    placeholder="Busca algo..."
                                                    className="mt-2"
                                                />
                                                <InputSubmit disabled>Buscar</InputSubmit>
                                            </div>


                                            <table className="table table-bordered table-hover w-100">

                                                <thead className="thead-dark text-center p-3">
                                                    <tr className="m-2">
                                                        <th className="align-middle">Seleccionar Abogado</th>
                                                        <th className="align-middle">Nombre y Apellidos</th>
                                                        <th className="align-middle">Tipo</th>
                                                        <th className="align-middle">Especialidades</th>
                                                        <th className="align-middle">Dirección</th>
                                                        <th className="align-middle">Última Oportunidad</th>
                                                        <th className="align-middle">Asuntos Llevados</th>
                                                        <th className="align-middle">Asuntos Rechazados</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    {resultados.length === 0 ? <SinResultados className="text-center" ><td colSpan="6">La búsqueda no dió resultados</td></SinResultados> : (


                                                        resultados.map(abogado => {

                                                            return (
                                                                <SeleccionarAbogado
                                                                    key={abogado.id}
                                                                    abogado={abogado}
                                                                    seleccionarRadio={seleccionarRadio}
                                                                    abogadoAsignado={abogadoAsignado}
                                                                    abogadosRechazadores={abogadosRechazadores}
                                                                />
                                                            )
                                                        })

                                                    )
                                                    }

                                                </tbody>

                                            </table>

                                        </>

                                    )}
                                {errores.abogadoAsignado ? (
                                    <p className="alert alert-danger text-center p-2 mt-2">{errores.abogadoAsignado}</p>
                                ) : null}
                                {error && <p className="alert alert-danger text-center p2 mt-2 mb-2">{error}</p>}
                                <div className="col-12 d-flex justify-content-center">
                                    <Link href="/">
                                        <BotonCancelar
                                            className="btn btn-secondary"
                                        >Cancelar</BotonCancelar>
                                    </Link>
                                    {rechazada ? (
                                        <BotonAceptar
                                            className="btn btn-primary prohibido"
                                            disabled
                                        >Guardar Cambios</BotonAceptar>

                                    ) : (
                                            <BotonAceptar
                                                className="btn btn-primary"
                                                type="submit"
                                            >Guardar Cambios</BotonAceptar>
                                        )}

                                </div>
                            </form>

                        </>
                    ) :
                        <>
                            <PaginaError msg={`Tienes que estar logueado para acceder a esta sección.`}></PaginaError>
                            <div className="text-center">
                                <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
                            </div>
                        </>
                )}

            </Layout>
        </div>
    );
}

export default Oportunidad;
