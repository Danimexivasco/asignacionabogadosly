import React, { useContext } from 'react';
import Link from 'next/link';

import Layout from '../components/layout/Layout';
import PaginaError from '../components/layout/PaginaError';

import {FirebaseContext} from '../firebase/index';

const Preguntas = () => {

  const {usuario} = useContext(FirebaseContext);

  return (
    <div>
      <Layout>

        {usuario ? (
          <>
            <h1>Preguntas</h1>

          </>
        ) :
          <>
            <PaginaError msg={`Tienes que estar logueado para acceder a esta sección.`}></PaginaError>
            <div className="text-center">
              <Link href="/iniciar-sesion"><a><h1>Inicia Sesión</h1></a></Link>
            </div>
          </>
        }
      </Layout>
    </div>
  )
}

export default Preguntas;
